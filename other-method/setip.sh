echo Setting IP to $1, Netmask to $2
cat <<EOF > /etc/sysconfig/network-scripts/ifcfg-ens192

#START SET-IP CONFIG BEGIN
IPADDR=$1
NETMASK=$2
GATEWAY=192.168.1.254
ONBOOT=yes
DEVICE=eth0
#FINAL SET-IP CONFIG BEGIN

EOF

#Restart networking to make IP active
/etc/init.d/network restart