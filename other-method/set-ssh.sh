#!/bin/bash

#we enable this to permit copy ssh public key on anible luncher with id and pwd -> playbook set-ssh.sh,
#then we disable this and we can run playbook without pswd but with the pub key who's copied on each nodes of cluster
#
sed -i -e "s/PasswordAuthentication no/PasswordAuthentication yes/g" /etc/ssh/sshd_config
systemctl restart sshd
