## Prerequis to controle a windows host with ansible
  - Configure windows server 2016 -> DHCP-DNS-AD
      https://hostadvice.com/how-to/how-to-install-an-openssh-server-client-on-a-windows-2016-server/

  - Install Openssh on windows server

  - Install WinRM on windows server
      https://www.ansible.com/blog/connecting-to-a-windows-host



## Prerequis to provisonning VM with vagrant on ESXI 6.7 :

# 0- Make env python 3.6 (Installing Python3 with SCL)
  # Update system

    sudo yum update

  # Install the centos-release-scl package 

    sudo yum install centos-release-scl

  # install the python3.6   

    sudo yum install rh-python36

  # launch Python36 in an new SCL shell, type

    scl enable rh-python36 bash


  # Install virtual environement on Centos7

    sudo yum install python-virtualenv

  # Create virtual env

    virutalenv env

  # launch your env

    source /env/bin/activate
  
  # Install ansible  
    
    pip install ansible

  
# 1- Donwload Vscode and Git 

## 1.1 Vscode (optional)

  # download rpm of vscode then

    sudo yum install code-1.43.0-1583783294.el7.x86_64.rpm

## 1.2 Git

  # You can use WANDisco's CentOS repository to install Git 2.x

    yum install http://opensource.wandisco.com/centos/6/git/x86_64/wandisco-git-release-6-1.noarch.rpm

    or

    yum install http://opensource.wandisco.com/centos/7/git/x86_64/wandisco-git-release-7-1.noarch.rpm

    or 

    yum install http://opensource.wandisco.com/centos/7/git/x86_64/wandisco-git-release-7-2.noarch.rpm

  # Install the latest version of Git 2.x

    yum install git

# 2- Ansible (Optional), newly we donwload ansible with python.

## 2.1- Download Ansible

 # if you look for a specefic version, choose the good version from https://releases.ansible.com/ansible/rpm/release/epel-7-x86_64/, download, then install it with the rpm.

    sudo yum code-1.43.0-1583783294.el7.x86_64.rpm  

 # else 

    sudo yum install ansible

## 2.2- Install mitogen 0.2.9 ( optional )

  # Donwload mitogen from this website then extract the .gz then copy the dir into okd-core
  
    https://mitogen.networkgenomics.com/ansible_detailed.html
    tar -xzvf mitogen-0.2.9.tar.gz
    cp mitogen-0.2.9 ~/work/okd-core

## 2.3- Modify ansible.cfg: 
    [defaults]
    strategy_plugins = /path/to/mitogen-0.2.9/ansible_mitogen/plugins/strategy
    strategy = mitogen_linear


# 3- Plugin-in Vagrant-vmware-esxi

## 3.1- Dowload Vagrant
  #  Download rpm https://vagrant.fr, then

    yum install vagrant vagrant_2.2.7_x86_64.rpm

## 3.2- Install ovftool
  # Download ovf from VMware website ; https://www.vmware.com/support/developer/ovf/, then

     sudo /bin/sh VMware-ovftool-4.3.0-7948156-lin.x86_64.bundle

## 3.3- Install the plugin  "vagrant-vmwre-esxi"
  # Il faut tout faire en sudo pour que cela fonctionne.
  
    sudo vagrant plugin install vagrant-vmware-esxi
    sudo vagrant plugin list
    sudo vagrant version

# 4- Ansible Playbook 

## 4.1- Set static ip and dns on all hosts (you should run this playbook 2 times, because we restart the svc 'network' on all hosts)
    ansible-playbook setup-ip-dns.yaml

## 4.2- Create user "origin" with all prerequis
    ansible-playbook prerequis-cluster.yaml


# 5- INSTALL OKD 3.11.+43

## 5.1 Configure hosts on each nodes

    ### [origin@clu-master-001 openshift-ansible]$ cat /etc/hosts

    127.0.0.1       clu-master-001.infrasrc.local  clu-master-001
    127.0.0.1   clu-master-001 clu-master-001.infrasrc.local
    ::1         clu-master-001 clu-master-001.infrasrc.local

    127.0.0.1 clu-master-001.infrasrc.local
    192.168.1.4 clu-master-001.infrasrc.local
    192.168.1.5  clu-node-001.infrasrc.local
    192.168.1.3 clu-node-002-infrasrc.local

    ### [origin@clu-node-001 ~]$ cat /etc/hosts  
    127.0.0.1	clu-node-001.infrasrc.local	clu-node-001
    127.0.0.1   clu-node-001 clu-node-001.infrasrc.local
    ::1         clu-node-001 clu-node-001.infrasrc.local

    127.0.0.1 clu-node-001.infrasrc.local
    192.168.1.4 clu-master-001.infrasrc.local
    192.168.1.5  clu-node-001.infrasrc.local
    192.168.1.3 clu-node-002-infrasrc.local

    ### [origin@clu-node-002 ~]$ cat /etc/hosts
    127.0.0.1	clu-node-002.infrasrc.local	clu-node-002
    127.0.0.1   clu-node-002 clu-node-002-infrasrc.local
    ::1         clu-node-002 clu-node-002-infrasrc.local

    127.0.0.1    clu-node-002-infrasrc.local
    192.168.1.4 clu-master-001.infrasrc.local
    192.168.1.5  clu-node-001.infrasrc.local
    192.168.1.3 clu-node-002-infrasrc.local


## 5.2- Install Ansible on "MASTER". To use EPEL as a package source for Ansible:

 # Install the EPEL repository:

    yum -y install \
    https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rp

 # Disable the EPEL repository globally so that it is not accidentally used during later steps of the installation:

     sed -i -e "s/^enabled=1/enabled=0/" /etc/yum.repos.d/epel.repo

 # Install the packages for Ansible:

     yum -y --enablerepo=epel install ansible pyOpenSSL

 # reboot all nodes

    sudo reboot
    
  # Not necessary but you can test if you got error from installation openshift ( optional )

    yum -y update

    yum -y install wget git net-tools bind-utils iptables-services bridge-utils bash-completion kexec-tools sos psacct


## 5.3- Download openshift-ansible from github

    cd ~
    git clone https://github.com/openshift/openshift-ansible
    cd openshift-ansible
    git checkout release-3.11

## 5.4- inventory openshift

    sudo vim /etc/ansible/hosts


    [OSEv3:children]
    masters
    nodes
    etcd

    [OSEv3:vars]
    ansible_ssh_user=origin
    ansible_become=true
    openshift_deployment_type=origin
    openshift_release="3.11"
    openshift_master_default_subdomain = apps.infrasrc.local
    openshift_disable_check=docker_storage
    os_firewall_use_firewalld = true
    openshift_install_examples = true
    openshift_docker_insecure_registries=172.30.0.0/16
    openshift_master_identity_providers = [{'name': 'htpasswd_auth', 'login': 'true', 'challenge': 'true', 'kind': 'HTPasswdPasswordIdentityProvider'}]
    openshift_master_cluster_hostname=clu-master-001.infrasrc.local
    openshift_master_cluster_public_hostname=clu-master-001.infrasrc.local


    [masters]
    clu-master-001.infrasrc.local

    [nodes]
    clu-master-001.infrasrc.local openshift_node_group_name="node-config-master-infra"
    clu-node-001.infrasrc.local openshift_node_group_name="node-config-compute"
    clu-node-002.infrasrc.local openshift_node_group_name="node-config-compute"

    [etcd]
    clu-master-001.infrasrc.local

## 5.5- Create user Htpasswd 
    htpasswd -c /etc/origin/master/htpasswd admin

    oc adm policy add-cluster-role-to-user cluster-admin admin


## 5.6- Create CNAME on srv-infra-001 

  # the router openshift is installd  on srv-master-001 so you have to do a record dns  from  openshift_master_default_subdomain  to  srv-master-001.infrasrc.local

    *.apps.infrasrc.local  CNAME  srv-master-001.infrasrc.local
    

# 6 - Playbook

## 6.1 Modify files 'hosts' and 'vars.yml'

## 6.2 Playbook install.yaml

  ansible-playbook install.yaml


## TODO ##


# INFO !!
  - work only on env : source /env/bin/activate
  - all command vagrant must be with "sudo"

    when you rebuilt box with same credential : 
    - delete old ssh in /home/remote/.ssh/known_hosts
    - connect first on ssh one by one
    - then playbook copy_ssh-publickey.yaml
    

# Automatisatte OKD : Adventure
 DONE- blockage : chmod 500 -> 700 dossier .ssh on master
 DONE- TODO - > test command: ansible all -m ping before launch deploycluster and preqresuist.
 DONE- blockage ->> TODO -> template the file ansible.cf on masster => host_key_cheking = False  ( go check the actually file on master )
 Done ->> TODO To fix htpasswd user on master, I just did a 
  on master :
  $ yum install python-pip
  $ pip install passlib

 Done -> TODO Change name of playbooks -> like prequis, etc..

